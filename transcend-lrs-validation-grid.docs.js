/**
 * @ngdoc overview
 * @name transcend.lrs-validation-grid
 * @description
 # LRS Validation Grid Module
 The LRS Validation Grid Module provides an interface for handling conflicts in segment data produced by segment analyzer.

 ## Functionality
 This module works with tds services to store the status of conflicts in a Validation Error Record.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-lrs-validation-grid.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-lrs-validation-grid/get/master.zip](http://code.tsstools.com/bower-lrs/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-lrs-validation-grid](http://code.tsstools.com/bower-lrs-validation-grid)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.lrs-validation-editor']);
 ```

 ## To Do
 - Complete 100% unit test coverage.
 */
/**
 * @ngdoc object
 * @name transcend.lrs-validation-grid.lrsGridEditorConfig
 *
 * @description
 * Provides a single configuration object that can be used to change the behaviour, options, urls, etc for any
 * components in the {@link transcend.lrs LRS Module}. The "lrsConfig" object is a
 * {@link http://docs.angularjs.org/api/auto/object/$provide#value module value} and can therefore be
 * overridden/configured by:
 *
 <pre>
 // Override the value of the 'esriConfig' object.
 angular.module('myApp').value('lrsConfig', {
     lrsProfile: {
       url: 'http://some-other-service/lrs'
     }
   });
 </pre>
 **/
/**
 * @ngdoc directive
 * @name transcend.lrs-validation-grid.lrsValidationGrid
 *
 * @description
 * A wrapper for {@link transcend.dynamic-grid.dynamicGrid}, specifically for visualizing and managing conflicts in
 * segment data.
 *
 * @restrict E
 * @element ANY
 *
 * @param {object} esriData The data to visualize -- must be the return from a MapServer query
 * @param {boolean} showControls Whether or not to show the map controls
 * @param {object} [mapManager] The RCE MapManager instance
 * @param {function} zoomFactory A function which returns a function that zooms the map. It must be a factory so the
 * scope can be injected into the function it returns
 *
 * @requires $http
 * @requires $filter
 * @requires transcend.core.$string
 * @requires lrsDataGridConfig
 * @requires transformLrsData
 * @requires localStorageService
 * @requires $log
 **/
