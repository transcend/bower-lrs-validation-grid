 # LRS Validation Grid Module
 The LRS Validation Grid Module provides an interface for handling conflicts in segment data produced by segment analyzer.

 ## Functionality
 This module works with tds services to store the status of conflicts in a Validation Error Record.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-lrs-validation-grid.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-lrs-validation-grid/get/master.zip](http://code.tsstools.com/bower-lrs/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-lrs-validation-grid](http://code.tsstools.com/bower-lrs-validation-grid)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.lrs-validation-editor']);
 ```

 ## To Do
 - Complete 100% unit test coverage.